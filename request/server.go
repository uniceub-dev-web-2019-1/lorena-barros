package main


import (
    "net/http"
    "time"
    "log"
)

// starts server from the s pointer created by servServ()
func ServerOn(){

	s:= servServ()

	//in case something goes wrong when initializing s, error will be logged
	log.Fatal(s.ListenAndServe())
}



/*
creates a server and returns s as pointer to a type Server
you may change the server address in constants' file
*/

func servServ (s *http.Server){

	s = &http.Server{Addr: SERVER_ADDR,
						  Handler: createRouter(),
						  ReadTimeout:  100 * time.Millisecond,
						  WriteTimeout: 200 * time.Millisecond,
						  IdleTimeout:  50 * time.Millisecond,
					}
	
	//returns pointer to s
	return
}

