package main

type Room struct {

	RoomCode int    	 `json:"roomCode"`
	RoomName string      `json:"roomName"`

}

func RegisterRoom(w http.ResponseWriter, req *http.Request){

	var room Room

	//var body is going to hold json's request body
	body := req.Body

	//readAll receives a body and returns a slice of bytes and an error
	byte,_ := ioutil.readAll(body)
		if err != nil { 	
	    	log.Println("Room ReadBody deu ruim: ", err)
	   		return
		}	
	/* Unmarshall receives []byte from the requests' body and an interface{}, 
	which in this case is room, and returns an error
	*/

	json.Unmarshall(bytes, &room)
		if err != nil { 	
	    	log.Println("Room unmarshal deu ruim: ", err)
	   		return
		}	


}


//check if room is available from the user's choices
func RoomAvailable()
