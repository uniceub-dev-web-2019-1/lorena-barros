package main

import ( "net/http"
		 "github.com/gorilla/mux" )


func createRouter() (handler *mux.Router) {

	// creates router
	handler = mux.NewRouter()


	// whenever a requests comes along, handler is going to rout that to the right path 

	/*
	when a user wishes to make reservations for a space, then that requests is going 
	to be handled by func ReserveUser, but it has to come with a post method
	*/
	
	handler.HandleFunc(RESERVE_PATH, ReserveRoom).Methods(http.MethodPost)

	// returns a pointer to handler
	return
}
