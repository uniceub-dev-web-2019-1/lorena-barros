package main

// services user has bought 

type Manager struct {

	Convenience Convenience
	Quantity int

}


// this has to func like a tab, with all the services the user has gotten so far, from time to time has to be updated

type ManagerTab struct {
	
	UserId int
	OpenTab []Manager
}