package main

import ("log"
		"time"
		"net/http"
		"io/ioutil"
		"encoding/json"
		)


//reservation for available rooms

type Reservation struct {
	
	UserId int 	     `json:"userId"`
	RoomCode int   	 `json:"roomCode"`
	Year int         `json:"year"`  //mudar isso ai tudo pra Time, tem fun JSON já na biblioteca
	Month Month      `json:"month"`
	Day int          `json:"day"`
	Hour int         `json:"hour"`
	Min int          `json:"minute"`
	
}

/* by http protocol every request must give a response
this function receives a json doc, reads it and returns the data necessary to instance a Reservation
*/
func ReserveRoom (w http.ResponseWriter, req *http.Request){

	// declares this variable, which is later gonna be instantiated with the requests's body info
	var reserve Reservation 

	//var body is going to hold json's request body
	body := req.Body

	//readAll receives a body and returns a slice of bytes and an error
	byte,_ := ioutil.readAll(body)
		if err != nil { 	
	    	log.Println("Reserve ReadBody deu ruim: ", err)
	   		return
		}	
	/* Unmarshall receives []byte from the requests' body and an interface{}, 
	which in this case is reserve, and returns an error
	*/

	json.Unmarshall(bytes, &reserve)
		if err != nil { 	
	    	log.Println("Reserve unmarshal deu ruim: ", err)
	   		return
		}	

}
