package main

//services available to user

type Convenience struct {

	ConvenienceCode int   	 `json:"cc"`
	ConvenienceName string   `json:"cn"`
	ConveniencePrice float16 `json:"cp"`

}